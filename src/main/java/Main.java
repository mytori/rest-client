import client.TeacherClient;
import client.TeacherClientImpl;
import model.Lesson;
import model.Teacher;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by Admin on 03.11.2017.
 */
public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        TeacherClient client = new TeacherClientImpl();
        Lesson lesson1 = new Lesson(1, "rest", 60);
        Lesson lesson2 = new Lesson(2, "soap", 60);
        Lesson lesson3 = new Lesson(3, "xml", 90);
        Teacher teacher1 = new Teacher(1, "Stepan", new Date(), new ArrayList<>());
        Teacher teacher2 = new Teacher(2, "Obivan", new Date(), new ArrayList<>());
        teacher1.getLessons().add(lesson3);
        teacher2.getLessons().add(lesson1);
        teacher2.getLessons().add(lesson2);
        try {
            client.doPost(teacher1);
            client.doPost(teacher2);
            System.out.println(client.doGet(1));
            System.out.println(client.doGet(2));
            System.out.println("Busiest - " + client.doGetBusiestTeacher());
            client.doPut(1, teacher1);
            client.doDelete(2,2);
            System.out.println("Busiest - " + client.doGetBusiestTeacher());
            client.doPut(2, new Teacher());
            System.out.println(client.doGet(2));
            client.doDelete(1);
            client.doDelete(2);
            System.out.println("Busiest - " + client.doGetBusiestTeacher());
        } catch (JAXBException | IOException e) {
            LOGGER.info(e.getMessage());
        }
    }
}
