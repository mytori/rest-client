package client;

import model.Lesson;
import model.Teacher;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jackson.map.ObjectMapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

/**
 * Created by Admin on 03.11.2017.
 */
public class TeacherClientImpl implements TeacherClient {
    private static final Logger LOGGER = Logger.getLogger(TeacherClientImpl.class.getName());

    private static final String SERVICE_URL = "http://localhost:8080/restful-soap/rest";
    private static final  String TEACHER_RESOURCE = "/teacher/";
    private static final String LESSON_RESOURCE = "/lesson/";
    private final HttpClient client;
    private static final String CONTENT_JSON = "application/json";

    private static final int HTTP_CODE_CREATED = 201;
    private static final int HTTP_CODE_NOT_MODIFIED = 304;
    private static final int HTTP_CODE_NOT_FOUND = 404;

    public TeacherClientImpl() {
        client = HttpClientBuilder.create().build();
    }
    public Teacher doGet(long teacherId) {
        String requestUrl = String.format(SERVICE_URL + "%s%d",TEACHER_RESOURCE, teacherId);
        HttpGet request = new HttpGet(requestUrl);
        try {
            LOGGER.info("Do GET request : " + requestUrl);
            HttpResponse response = client.execute(request);
            LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() == HTTP_CODE_NOT_FOUND) {
                return null;
            }
            return getTeacherFromInputStream(response);
        } catch (IOException e) {

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Teacher doPost(Teacher teacher) throws JAXBException, IOException {
        String requestUrl = SERVICE_URL + TEACHER_RESOURCE;
        HttpPost request = new HttpPost(requestUrl);
        StringEntity requestData = new StringEntity(convertInJson(teacher), StandardCharsets.UTF_8);
        requestData.setContentType(CONTENT_JSON);
        request.setEntity(requestData);
        LOGGER.info("Do POST request : " + requestUrl);
        HttpResponse response = client.execute(request);
        LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
        return getTeacherFromInputStream(response);
    }

    public void doPut(long teacherId, Teacher newTeacher) throws IOException {
        String requestUrl = String.format(SERVICE_URL + "%s%d",TEACHER_RESOURCE, teacherId);
        HttpPut request = new HttpPut(requestUrl);
        StringEntity requestData = new StringEntity(convertInJson(newTeacher), StandardCharsets.UTF_8);
        requestData.setContentType(CONTENT_JSON);
        request.setEntity(requestData);
        LOGGER.info("Do PUT request : " + requestUrl);
        HttpResponse response = client.execute(request);
        LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
    }

    public void doPut(long teacherId, long lessonId, Lesson newLesson) throws IOException {
        String requestUrl = String.format(SERVICE_URL + "%s%d%s%d", TEACHER_RESOURCE, teacherId, LESSON_RESOURCE, lessonId);
        HttpPut request = new HttpPut(requestUrl);
        StringEntity requestData = new StringEntity(convertInJson(newLesson), StandardCharsets.UTF_8);
        requestData.setContentType(CONTENT_JSON);
        request.setEntity(requestData);
        LOGGER.info("Do PUT request : " + requestUrl);
        HttpResponse response = client.execute(request);
        LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
    }

    public void doDelete(long teacherId, long lessonId) throws IOException {
        String requestUrl = String.format(SERVICE_URL + "%s%d%s%d", TEACHER_RESOURCE, teacherId, LESSON_RESOURCE, lessonId);
        HttpDelete request = new HttpDelete(requestUrl);
        LOGGER.info("Do DELETE request " + requestUrl);
        HttpResponse response = client.execute(request);
        LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
    }

    public void doDelete(long teacherId) throws IOException {
        String requestUrl = String.format(SERVICE_URL + "%s%d",TEACHER_RESOURCE, teacherId);
        HttpDelete request = new HttpDelete(requestUrl);
        LOGGER.info("Do DELETE request " + requestUrl);
        HttpResponse response = client.execute(request);
        LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
    }

    @Override
    public Teacher doGetBusiestTeacher() throws JAXBException, IOException {
        String requestUrl = String.format(SERVICE_URL + "%s%s",TEACHER_RESOURCE, "busiest" );
        HttpGet request = new HttpGet(requestUrl);
        LOGGER.info("Do GET request : " + requestUrl);
        HttpResponse response = client.execute(request);
        LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
        if (response.getStatusLine().getStatusCode() == HTTP_CODE_NOT_FOUND) {
            return null;
        }
        return getTeacherFromInputStream(response);
    }

    private static <T> String convertInJson(T object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }

    private Teacher getTeacherFromInputStream(HttpResponse response) throws JAXBException, IOException {

        JAXBContext jaxbContext = JAXBContext.newInstance(Teacher.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (Teacher) jaxbUnmarshaller.unmarshal(response.getEntity().getContent());
    }
}
