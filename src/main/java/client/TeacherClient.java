package client;

import model.Lesson;
import model.Teacher;

import javax.xml.bind.JAXBException;
import java.io.IOException;

/**
 * Created by Admin on 03.11.2017.
 */
public interface TeacherClient {
    public Teacher doGet(long teacherId);

    public Teacher doPost(Teacher teacher) throws JAXBException, IOException;

    public void doPut(long teacherId, Teacher newteacher) throws IOException;

    public void doPut(long teacherId, long lessonId, Lesson newLesson) throws IOException;

    public void doDelete(long teacherId, long lessonId) throws IOException;

    public void doDelete(long teacherId) throws IOException;

    public Teacher doGetBusiestTeacher() throws JAXBException, IOException;
}
